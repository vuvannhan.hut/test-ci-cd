<?php

return [
    'status' => [
        0 => 'Submit',
        1 => 'Interview',
        2 => 'Reject',
        3 => 'Offer'
    ]
];
