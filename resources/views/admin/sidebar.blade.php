<div class="col-md-3">
    <div>
        @foreach($laravelAdminMenus->menus as $section)
            @if($section->items)
                <div class="card">
                    <div class="card-header">
                        <button type="button" data-toggle="collapse" data-target="#navbarSupportedResource"
                                aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation"
                                class="navbar-toggler">
                            {{ $section->section }}
                        </button>
                    </div>
                    <div class="card-body" id="navbarSupportedResource" class="navbar-collapse collapse show" style="">
                        <ul class="nav flex-column" role="tablist">
                            @foreach($section->items as $menu)
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="{{ url($menu->url) }}">
                                        {{ $menu->title }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <br/>
            @endif
        @endforeach
    </div>
</div>
