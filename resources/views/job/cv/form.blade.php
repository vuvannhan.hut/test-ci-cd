<div class="form-group{{ $errors->has('company_id') ? 'has-error' : ''}}">
    {!! Form::label('company_id', 'Company Id', ['class' => 'control-label']) !!}
    {!! Form::select('company_id', $companies, null, ['class' => 'form-control']) !!}
    {!! $errors->first('company_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('submit_date') ? 'has-error' : ''}}">
    {!! Form::label('submit_date', 'Submit Date', ['class' => 'control-label']) !!}
    {!! Form::date('submit_date', Carbon\Carbon::today()->format('Y-m-d'), ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('submit_date', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('status_id') ? 'has-error' : ''}}">
    {!! Form::label('status_id', 'Status Id', ['class' => 'control-label']) !!}
    {!! Form::select('status_id', $statuses, null, ['class' => 'form-control']) !!}
    {!! $errors->first('status_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('salary') ? 'has-error' : ''}}">
    {!! Form::label('salary', 'Salary', ['class' => 'control-label']) !!}
    {!! Form::number('salary', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('salary', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
