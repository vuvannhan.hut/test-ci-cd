<div class="form-group{{ $errors->has('agent_name') ? 'has-error' : ''}}">
    {!! Form::label('agent_name', 'Agent Name', ['class' => 'control-label']) !!}
    {!! Form::text('agent_name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('agent_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('agent_code') ? 'has-error' : ''}}">
    {!! Form::label('agent_code', 'Agent Code', ['class' => 'control-label']) !!}
    {!! Form::text('agent_code', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('agent_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('agent_phone') ? 'has-error' : ''}}">
    {!! Form::label('agent_phone', 'Agent Phone', ['class' => 'control-label']) !!}
    {!! Form::text('agent_phone', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('agent_phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('agent_address') ? 'has-error' : ''}}">
    {!! Form::label('agent_address', 'Agent Address', ['class' => 'control-label']) !!}
    {!! Form::text('agent_address', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('agent_address', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
