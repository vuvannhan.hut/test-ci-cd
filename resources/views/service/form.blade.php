<div class="form-group{{ $errors->has('agent_id') ? 'has-error' : ''}}">
    {!! Form::label('agent_id', 'Agent Id', ['class' => 'control-label']) !!}
    {!! Form::text('agent_id', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('agent_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('service_name') ? 'has-error' : ''}}">
    {!! Form::label('service_name', 'Service Name', ['class' => 'control-label']) !!}
    {!! Form::text('service_name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('service_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('service_code') ? 'has-error' : ''}}">
    {!! Form::label('service_code', 'Service Code', ['class' => 'control-label']) !!}
    {!! Form::text('service_code', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('service_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('service_price') ? 'has-error' : ''}}">
    {!! Form::label('service_price', 'Service Price', ['class' => 'control-label']) !!}
    {!! Form::number('service_price', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('service_price', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('service_description') ? 'has-error' : ''}}">
    {!! Form::label('service_description', 'Service Description', ['class' => 'control-label']) !!}
    {!! Form::textarea('service_description', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('service_description', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
