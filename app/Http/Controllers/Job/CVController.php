<?php

namespace App\Http\Controllers\Job;

use App\Company;
use App\Http\Controllers\Controller;

use App\CV;
use App\Status;
use Illuminate\Http\Request;

class CVController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $cv = CV::where('submit_date', 'LIKE', "%$keyword%")
                ->orWhere('salary', 'LIKE', "%$keyword%")
                ->orWhereHas('company', function ($query) use ($keyword) {
                    $query->where('company_name', 'LIKE', "%$keyword%");
                })
                ->orWhereHas('status', function ($query) use ($keyword) {
                    $query->where('status', 'LIKE', "%$keyword%");
                })
                ->latest()->paginate($perPage);
        } else {
            $cv = CV::latest()->paginate($perPage);
        }

        return view('job.cv.index', compact('cv'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $companies = Company::all()->pluck('company_name', 'id')->toArray();
        $statuses = Status::all()->pluck('status', 'id')->toArray();

        return view('job.cv.create', compact('companies', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company_id' => 'required',
            'submit_date' => 'required',
            'status_id' => 'required',
            'salary' => 'required'
        ]);
        $requestData = $request->all();

        CV::create($requestData);

        return redirect('job/cv')->with('flash_message', 'CV added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $cv = CV::findOrFail($id);
        $companies = Company::all()->pluck('company_name', 'id')->toArray();
        $statuses = Status::all()->pluck('status', 'id')->toArray();

        return view('job.cv.show', compact('cv', 'companies', 'statuses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $cv = CV::findOrFail($id);
        $companies = Company::all()->pluck('company_name', 'id')->toArray();
        $statuses = Status::all()->pluck('status', 'id')->toArray();

        return view('job.cv.edit', compact('cv', 'companies', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'company_id' => 'required',
            'submit_date' => 'required',
            'status_id' => 'required',
            'salary' => 'required'
        ]);
        $requestData = $request->all();

        $cv = CV::findOrFail($id);
        $cv->update($requestData);

        return redirect('job/cv')->with('flash_message', 'CV updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        CV::destroy($id);

        return redirect('job/cv')->with('flash_message', 'CV deleted!');
    }
}
