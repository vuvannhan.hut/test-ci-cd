<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Agent;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $agent = Agent::where('agent_name', 'LIKE', "%$keyword%")
                ->orWhere('agent_code', 'LIKE', "%$keyword%")
                ->orWhere('agent_phone', 'LIKE', "%$keyword%")
                ->orWhere('agent_address', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $agent = Agent::latest()->paginate($perPage);
        }

        return view('agent.index', compact('agent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('agent.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'agent_name' => 'required',
            'agent_code' => 'required'
        ]);
        $requestData = $request->all();
        
        Agent::create($requestData);

        return redirect('agent')->with('flash_message', 'Agent added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $agent = Agent::findOrFail($id);

        return view('agent.show', compact('agent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $agent = Agent::findOrFail($id);

        return view('agent.edit', compact('agent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'agent_name' => 'required',
            'agent_code' => 'required'
        ]);
        $requestData = $request->all();
        
        $agent = Agent::findOrFail($id);
        $agent->update($requestData);

        return redirect('agent')->with('flash_message', 'Agent updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Agent::destroy($id);

        return redirect('agent')->with('flash_message', 'Agent deleted!');
    }
}
