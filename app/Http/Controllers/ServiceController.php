<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $service = Service::where('agent_id', 'LIKE', "%$keyword%")
                ->orWhere('service_name', 'LIKE', "%$keyword%")
                ->orWhere('service_code', 'LIKE', "%$keyword%")
                ->orWhere('service_price', 'LIKE', "%$keyword%")
                ->orWhere('service_description', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $service = Service::latest()->paginate($perPage);
        }

        return view('service.index', compact('service'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'agent_id' => 'required',
            'service_name' => 'required',
            'service_code' => 'required'
        ]);
        $requestData = $request->all();
        
        Service::create($requestData);

        return redirect('service')->with('flash_message', 'Service added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $service = Service::findOrFail($id);

        return view('service.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $service = Service::findOrFail($id);

        return view('service.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'agent_id' => 'required',
            'service_name' => 'required',
            'service_code' => 'required'
        ]);
        $requestData = $request->all();
        
        $service = Service::findOrFail($id);
        $service->update($requestData);

        return redirect('service')->with('flash_message', 'Service updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Service::destroy($id);

        return redirect('service')->with('flash_message', 'Service deleted!');
    }
}
